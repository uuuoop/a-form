"use client";

import Image from "next/image";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import MainCpm from "./cpm/MainCpm";
import { getPersonList } from "./apis/axios"
import { useState, useEffect } from "react";

const theme = createTheme({
  palette: {
    primary: { main: '#00bcd4' },
    secondary: { main: '#00e5ff' },
  },
});

export default function Home() {
  const [apersonList, setAPersonList] = useState(null)

  useEffect(() => {
    getPersonList().then(val => {
      console.log(val)

      setAPersonList(val.data
        .map(val => {
          delete val._id
          delete val.__v
          return val
        })
      )

    })
  }, [])

  return (
    <ThemeProvider theme={theme}>
      <main className="h-dvh flex min-h-screen flex-col items-center justify-between p-24">
        {apersonList && <MainCpm apersonList={apersonList} />}
      </main>
    </ThemeProvider>
  );
}
