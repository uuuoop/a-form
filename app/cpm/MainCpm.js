"use client";

import * as React from "react";
import Button from "@mui/material/Button";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import DeleteIcon from "@mui/icons-material/Delete";
import { TransitionGroup } from "react-transition-group";
import AddIcon from "@mui/icons-material/Add";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import TextField from "@mui/material/TextField";
import { v4 as uuidv4 } from "uuid";
import { zhCN } from "@mui/x-date-pickers/locales";
import "dayjs/locale/zh-cn";
import Alert from "@mui/material/Alert";
import CheckIcon from "@mui/icons-material/Check";
import Snackbar from "@mui/material/Snackbar";
import { debounce } from "lodash";
import dayjs from "dayjs";
dayjs.locale('zh-cn')
import InputFileUpload from "./InputFileUpload";
import {
  getPersonList,
  createPerson,
  upDataPerson,
  deletePerson,
} from "@/app/apis/axios";

function renderItem({
  indexN,
  item,
  handleRemovePerson,
  activeIndex,
  setActiveIndex,
  setActiveIndexN,
  activeIndexN,
}) {
  return (
    <ListItem
      className={` transition ${activeIndex !== item.uid && "hover:border-cyan-200"
        } hover:scale-105 hover:-translate-y-1 flex hover:cursor-pointer  bg-white mt-3 rounded-lg border-2 ${activeIndex === item.uid ? "border-cyan-400" : " border-gray-200"
        } `}
      onClick={() => {
        setActiveIndex(item.uid);
        setActiveIndexN(indexN);
      }}
    >
      <p className="flex-1">{item.name}</p>
      <IconButton
        edge="end"
        aria-label="delete"
        title="删除"
        onClick={(e) => {
          e.stopPropagation();
          handleRemovePerson(item, indexN);
        }}
      >
        <DeleteIcon />
      </IconButton>
    </ListItem>
  );
}

export default function MainCpm({ apersonList }) {
  const [personList, setPersonList] = React.useState(apersonList);
  const [activeIndex, setActiveIndex] = React.useState(null);
  const [activeIndexN, setActiveIndexN] = React.useState(-1);
  const [open, setOpen] = React.useState(false);
  const changePerson = new Set();

  const handleOpen = () => {
    setOpen(true);
    setTimeout(() => {
      setOpen(false);
    }, 3000); // 3秒后自动关闭
  };

  const debouncedHandleInputChange = debounce(() => {
    const changePersonList = [...changePerson];
    upDataPerson(changePersonList).then(val=>handleOpen());
    console.log(changePerson)
    changePerson.clear();
  }, 3000);

  const onChangeEdit = () => {
    changePerson.add(personList[activeIndexN]);
    console.log(changePerson)
    debouncedHandleInputChange();
  };

  // React.useEffect(()=>{
  //   changePerson.add(personList[activeIndexN])
  //   debouncedHandleInputChange()
  // }, [personList])

  const handleAddPerson = () => {
    const nId = uuidv4();
    setPersonList((prev) => [
      {
        uid: nId,
        name: "",
        personId: "",
        sex: "",
        avaerge: "",
        bornDate: dayjs(),
        joinDate: dayjs(),
        photoUrl: "",
        resumeUrl: "",
      },
      ...prev,
    ]);
    createPerson({ uid: nId })
    setActiveIndex(nId);
    setActiveIndexN(0);
  };

  const handleRemovePerson = (item, indexN) => {
    deletePerson(personList[indexN])
    setPersonList((prev) => prev.filter((val) => val.uid !== item.uid));

    if (indexN === activeIndexN) {
      console.log("remove", personList[0].uid);
      setActiveIndex(null);
      setActiveIndexN(-1);
    }
    if (indexN < activeIndexN) setActiveIndexN((pre) => pre - 1);
  };

  const addButton = (
    <div
      className="transition ease-in-out delay-150 hover:-translate-y-1 hover:scale-110 hover:bg-cyan-50 duration-300 hover:border-solid hover:cursor-pointer	border-dashed border-2 flex items-center justify-center rounded-lg bg-white mt-5 ml-5 mr-5 h-14 text-slate-900 font-medium "
      onClick={handleAddPerson}
    >
      <div className=" select-none" sx={{ p: "10px" }}>
        <AddIcon />
        新增员工信息
      </div>
    </div>
  );

  return (
    <div className="w-7/12 h-full bg-white flex rounded-lg border-2 border-gray-200 shadow-xl ">
      <div className="absolute z-0 top-0 inset-x-0 flex justify-center overflow-hidden pointer-events-none">
        <div className="w-[108rem] flex-none flex justify-end">
          <picture>
            <img src="/docs@30.8b9a76a2.avif" alt="" />
          </picture>
        </div>
      </div>
      <div className="backdrop-blur-xl z-10 flex-auto flex flex-col w-32 bg-cyan-50 rounded-l-lg">
        {addButton}
        <List sx={{ mt: 1, paddingLeft: '20px', paddingRight: '20px' }} className="flex-1 overflow-y-scroll pl-5 pr-5">
          <TransitionGroup>
            {personList.map((item, indexN) => (
              <Collapse key={item.uid}>
                {renderItem({
                  item,
                  handleRemovePerson,
                  activeIndex,
                  setActiveIndex,
                  indexN,
                  setActiveIndexN,
                  activeIndexN,
                })}
              </Collapse>
            ))}
          </TransitionGroup>
        </List>
      </div>

      <LocalizationProvider
        dateAdapter={AdapterDayjs}
        adapterLocale="zh-cn"
        localeText={
          zhCN.components.MuiLocalizationProvider.defaultProps.localeText
        }
      >
        <div className="z-10 flex-auto w-64 flex-col backdrop-blur-md bg-white/30 ">
          {activeIndexN !== -1 ? (
            <div className="grid justify-center	 pl-10 pr-10 pt-12 gap-y-5	">
              <div>
                <TextField
                  label="姓名"
                  variant="outlined"
                  value={personList[activeIndexN].name}
                  onChange={(e) => {
                    setPersonList((pre) => {
                      const cPerson = [...pre];
                      cPerson[activeIndexN].name = e.target.value;
                      return cPerson;
                    });
                    onChangeEdit()
                  }}
                  className="w-full	"
                />
              </div>
              <div>
                <TextField
                  label="身份证号"
                  variant="outlined"
                  value={personList[activeIndexN].personId}
                  onChange={(e) => {
                    setPersonList((pre) => {
                      const cPerson = [...pre];
                      cPerson[activeIndexN].personId = e.target.value;
                      return cPerson;
                    });
                    onChangeEdit()
                  }}
                  className="w-full	"
                />
              </div>
              <div>
              <FormControl>
                <FormLabel>性别</FormLabel>
                <RadioGroup
                  row
                  value={personList[activeIndexN].sex||''}
                  onChange={(e) => {
                    setPersonList((pre) => {
                      const cPerson = [...pre];
                      cPerson[activeIndexN].sex = e.target.value;
                      return cPerson;
                    });
                    onChangeEdit()
                  }}
                >
                  <FormControlLabel
                    value="female"
                    control={<Radio />}
                    label="男"
                  />
                  <FormControlLabel
                    value="male" 
                    control={<Radio />}
                    label="女"
                  />
                </RadioGroup>
                </FormControl>
              </div>
              <div>
                <TextField
                  label="工资"
                  type="number"
                  value={personList[activeIndexN].avaerge}
                  onChange={(e) => {
                    setPersonList((pre) => {
                      const cPerson = [...pre];
                      cPerson[activeIndexN].avaerge = e.target.value;
                      return cPerson;
                    });
                    onChangeEdit()
                  }}
                />
              </div>
              <div>
                <DatePicker label="出生日期"
                  value={dayjs(personList[activeIndexN].bornDate)}
                  onChange={(e) => {
                    setPersonList((pre) => {
                      console.log(e.toString())
                      const cPerson = [...pre];
                      cPerson[activeIndexN].bornDate = e;
                      return cPerson;
                    });
                    onChangeEdit()
                  }}
                />
              </div>
              <div>
                <DatePicker
                  localeText={
                    zhCN.components.MuiLocalizationProvider.defaultProps
                      .localeText
                  }
                  label="入职日期"
                  value={dayjs(personList[activeIndexN].joinDate)}
                  onChange={(e) => {
                    setPersonList((pre) => {
                      console.log(e.toString())
                      const cPerson = [...pre];
                      cPerson[activeIndexN].joinDate = e;
                      return cPerson;
                    });
                    onChangeEdit()
                  }}
                />
              </div>
            </div>
          ) : (
            <div></div>
          )}
        </div>
      </LocalizationProvider>
      <Snackbar
        open={open}
        autoHideDuration={3000}
        onClose={() => setOpen(false)}
        message="自动保存成功"
      />
    </div>
  );
}
