import axios from "axios";

const instance = axios.create({
    baseURL: 'http://124.71.66.47:2333',
    timeout: 5000
});

export function getPersonList () {
    return instance.get("/person_list")
}

export function createPerson(person){
    return instance.post("/create_person", {...person})
}

export function upDataPerson (person) {
    return instance.post("/update_person", {personList:person||[]})
}

export function deletePerson(person) {
    return instance.post("/delete_person", {person})
}
export function getUpdataLog() {
    return instance.post("/updata_list")
}
